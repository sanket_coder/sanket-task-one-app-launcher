package com.sankettaskone.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AppBehaviorReceiver extends BroadcastReceiver {
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        // when package removed
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            Toast.makeText(context, "Sanket Task One: PACKAGE_REMOVED",
                    Toast.LENGTH_LONG).show();
        }
        // when package installed
        else if (intent.getAction().equals(
                "android.intent.action.PACKAGE_ADDED")) {
            Toast.makeText(context, "Sanket Task One:" + "PACKAGE_ADDED",
                    Toast.LENGTH_LONG).show();
        }
    }
}

