package com.sankettaskone.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppInfo implements Comparable<AppInfo> {
    public String name;
    public String versionName;
    public Drawable icon;
    public String packageName;
    private int versionCode;
    private String mainClass;
    private long firstInstallTime;

    @SuppressLint("WrongConstant")
    public static List<AppInfo> getAppInfoList(Activity a) {
        ArrayList<AppInfo> appInfoList = new ArrayList<AppInfo>();
        List<PackageInfo> packs = a.getPackageManager().getInstalledPackages(PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
        final int size = packs.size();
        for (int i = 0; i < size; i++) {
            PackageInfo p = packs.get(i);
            if (p.versionName == null) {
                continue;
            }
            if ((p.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                AppInfo newInfo = new AppInfo();
                newInfo.name = p.applicationInfo.loadLabel(a.getPackageManager()).toString();
                newInfo.packageName = p.packageName;
                newInfo.versionName = p.versionName;
                newInfo.versionCode = p.versionCode;
                newInfo.firstInstallTime = p.firstInstallTime;
                newInfo.icon = p.applicationInfo.loadIcon(a.getPackageManager());
                newInfo.mainClass = p.applicationInfo.className;
                appInfoList.add(newInfo);
            }
        }
        Collections.sort(appInfoList);
        return appInfoList;
    }

    @Override
    public int compareTo(AppInfo appInfo) {
        return name.compareTo(appInfo.name);
    }
}


