package com.sankettaskone.app;

import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sankettaskone.R;
import com.sankettaskone.sdk.AppBehaviorReceiver;
import com.sankettaskone.sdk.AppInfo;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recy_view;
    LinearLayoutManager linearLayoutManager;
    AppInfoAdapter adapter;
    AppBehaviorReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setBroadcastReceiver();
        getAppList();
    }

    /*
    Initialize views
     */
    public void init() {
        recy_view = findViewById(R.id.recy_view);
        recy_view.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recy_view.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recy_view.getContext(),
                linearLayoutManager.getOrientation());
        recy_view.addItemDecoration(dividerItemDecoration);
    }

    /*
    Intialize broadcast receiver
     */
    public void setBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        receiver = new AppBehaviorReceiver();
        registerReceiver(receiver, intentFilter);
    }

    /*
    Get installed app list and set to adapter
     */
    private void getAppList() {
        List<AppInfo> appInfoList = AppInfo.getAppInfoList(this);
        adapter = new AppInfoAdapter(this);
        recy_view.setAdapter(adapter);
        adapter.setData(appInfoList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        MenuItem searchViewItem = menu.findItem(R.id.app_bar_search);
        final SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}