package com.sankettaskone.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sankettaskone.R;
import com.sankettaskone.sdk.AppInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AppInfoAdapter extends RecyclerView.Adapter<AppInfoAdapter.AppInfoViewHolder> {

    LayoutInflater inflater;
    List<AppInfo> infoList, searchAppList;
    Context context;

    public AppInfoAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        infoList = new ArrayList<>();
        searchAppList = new ArrayList<>();
    }

    @NonNull
    @Override
    public AppInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.appinfo_item, parent, false);
        return new AppInfoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AppInfoViewHolder holder, final int position) {
        holder.appName.setText(searchAppList.get(position).name);
        holder.appVersion.setText(searchAppList.get(position).versionName);
        holder.appIcon.setImageDrawable(searchAppList.get(position).icon);
        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchApp(searchAppList.get(position).packageName);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (searchAppList != null)
            return searchAppList.size();
        else
            return 0;
    }

    public void setData(List<AppInfo> list) {
        searchAppList = list;
        infoList = list;
        notifyDataSetChanged();
    }

    public void filter(String searchText) {
        if (TextUtils.isEmpty(searchText)) {
            searchAppList.clear();
            searchAppList.addAll(infoList);
            notifyDataSetChanged();
            return;
        }
        LinkedList<AppInfo> filterl = new LinkedList<>();
        for (int i = 0; i < infoList.size(); i++) {
            if (infoList.get(i).name.toLowerCase().contains(searchText.toLowerCase())) {
                filterl.add(infoList.get(i));
            }
        }
        filterList(filterl);
    }

    public void filterList(LinkedList<AppInfo> filterdList) {
        searchAppList = filterdList;
        notifyDataSetChanged();
    }

    public void launchApp(String packageName) {
        PackageManager pack = context.getPackageManager();
        Intent app = pack.getLaunchIntentForPackage(packageName);
        context.startActivity(app);
    }

    public class AppInfoViewHolder extends RecyclerView.ViewHolder {

        TextView appName, appVersion;
        ImageView appIcon;
        RelativeLayout rl_main;

        public AppInfoViewHolder(@NonNull View itemView) {
            super(itemView);
            appIcon = itemView.findViewById(R.id.appIcon);
            appName = itemView.findViewById(R.id.appName);
            appVersion = itemView.findViewById(R.id.appVersion);
            rl_main = itemView.findViewById(R.id.rl_main);
        }
    }
}
